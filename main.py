import sys
import json
import struct
import time
import commands
import subprocess
from PyQt4.QtGui import *
from PyQt4.QtCore import *

chosendevice = ''

def getlists():
	jsondevices = []
	jsonexams = []
	devices = commands.getoutput('ls acquisition').split('\n')
	exams = commands.getoutput('ls exams').split('\n')
	for device in devices:
		jsondevices.append(json.loads(commands.getoutput("cat \"acquisition/" + device + "/init\"")))
	for exam in exams:
		jsonexams.append(json.loads(commands.getoutput("cat \"exams/" + exam + "/init\"")))
	return (jsondevices, jsonexams)
	

class Window():
	def __init__(self):
		#Janela
		self.xstart = 300
		self.window = QDialog()
		self.window.resize(1360, 768)
		self.window.setWindowTitle('Registro de Pacientes')
		self.window.finished.connect(self.endapp)
		self.action = 0
		#Variaveis
		(self.devicelist, self.examlist) = getlists()
		self.chosendevice = None
		self.chosenexam = None
		#Titulo
		self.title = QLabel('Registro de Pacientes', self.window)
		font = QFont()
		font.setPointSize(40)
		font.setBold(True)
		self.title.setFont(font)
		self.title.move(400, 30)
		#Nome
		self.nametitle = QLabel('Nome', self.window)
		self.name = QLineEdit(self.window)
		self.name.resize(750, 30)
		self.nametitle.move(self.xstart, 130)
		self.name.move(self.xstart, 150)
		#Gender
		self.gendertitle = QLabel('Genero', self.window)
		self.gender = QComboBox(self.window)
		self.gender.addItem('Masculino')
		self.gender.addItem('Feminino')
		self.gendertitle.move(self.xstart, 190)
		self.gender.move(self.xstart, 210)
		#Nascimento
		self.birthdaytitle = QLabel('Data de Nascimento', self.window)
		self.birthday = QDateEdit(self.window)
		self.birthday.setDisplayFormat('dd/MM/yyyy')
		self.birthday.setDateRange(QDate(1900, 1, 1), QDate(2017, 11, 18))
		self.birthdaytitle.move(self.xstart+150, 190)
		self.birthday.move(self.xstart+150, 210)
		#RG
		self.rgtitle = QLabel('RG', self.window)
		self.rg = QLineEdit(self.window)
		self.rg.setMaxLength(12)
		self.rg.setInputMask("999999999999")
		self.rg.resize(200, 30)
		self.rgtitle.move(self.xstart+310, 190)
		self.rg.move(self.xstart+310, 210)
		#CPF
		self.cpftitle = QLabel('CPF', self.window)
		self.cpf = QLineEdit(self.window)
		self.cpf.setMaxLength(11)
		self.cpf.setInputMask("99999999999")
		self.cpf.resize(200, 30)
		self.cpftitle.move(self.xstart+550, 190)
		self.cpf.move(self.xstart+550, 210)
		#Aparelho
		self.devicetitle = QLabel('Aparelho', self.window)
		self.device = QComboBox(self.window)
		self.device.addItem('')
		
		for device in self.devicelist:
			self.device.addItem(device['Name'])
			
		self.device.resize(350, 30)
		self.devicetitle.move(self.xstart, 250)
		self.device.move(self.xstart, 270)
		#Exame
		self.examtitle = QLabel('Exame', self.window)
		self.exam = QComboBox(self.window)
		self.exam.addItem('')
		
		for exam in self.examlist:
			self.exam.addItem(exam['Name'])
			
		self.exam.resize(350, 30)
		self.examtitle.move(self.xstart+400, 250)
		self.exam.move(self.xstart+400, 270)
		#Comentarios
		self.commenttitle = QLabel('Comentarios', self.window)
		self.comment = QTextEdit(self.window)
		self.comment.resize(750, 300)
		self.commenttitle.move(self.xstart, 310)
		self.comment.move(self.xstart, 330)
		#Botao voltar
		self.backbutton = QPushButton('Voltar', self.window)
		self.backbutton.move(30, 30)
		self.backbutton.clicked.connect(self.back)
		#Botao avancar
		self.forwardbutton = QPushButton('Avancar', self.window)
		self.forwardbutton.move(1250, 30)
		self.forwardbutton.clicked.connect(self.forward)
	
	def back(self, event):
		self.action = 1
		app.exit()	
	
	def forward(self, event):
		if str(self.name.text()) == '' or len(self.rg.text()) < 8 or len(self.cpf.text()) < 11 or self.device.currentText() == '':
			self.dlg = QMessageBox(self.window)
			self.dlg.setWindowTitle("Aviso")
			self.dlg.setIcon(QMessageBox.Question)
			self.dlg.setText("Preencha todos os campos corretamente")
			self.dlg.exec_()
		else:
			if self.exam.currentText() != '':
				for exam in self.examlist:
					if exam['Name'] == self.exam.currentText():
						self.chosenexam = exam	
				for device in self.devicelist:
					if device['Name'] == self.device.currentText():
						self.chosendevice = device
				if self.chosenexam:
					if self.chosenexam['fanin'] != self.chosendevice['fanout']:
						self.dlg = QMessageBox(self.window)
						self.dlg.setWindowTitle("Aviso")
						self.dlg.setIcon(QMessageBox.Question)
						self.dlg.setText("Aparelho e Exame Incompativeis")
						self.dlg.exec_()
						self.chosenexam = None
						self.chosendevice = None
					else:
						app.exit()
				else:
					app.exit() 
	
	def endapp(self, event):
		app.exit()
				
	
	
		
if __name__ == '__main__':
	app = QApplication(sys.argv)
	main = Window()
	main.window.show()
	app.exec_()
	main.window.close()
	if main.chosendevice:
		subprocess.Popen('python ' + main.chosendevice['File'], shell=True)
		userfile = open('temp/user.txt', 'w')
		userinformation = {'Name' : str(main.name.text()), 'CPF': str(main.cpf.text()), 'RG': str(main.rg.text()), 'Birthday': '{0} {1} {2}'.format(int(main.birthday.date().year()), 
							int(main.birthday.date().month()), int(main.birthday.date().day())), 'Comment' : str(main.comment.toPlainText()), 'Device': main.chosendevice, 
							'Exam': main.chosenexam, 'Gender': str(main.gender.currentText())}
		json.dump(userinformation, userfile)
		userfile.close()
	elif main.action == 1:
		subprocess.Popen('python selection.py', shell=True)
	sys.exit()
