import sys
import json
import struct
import time
import commands
import subprocess
from database import userexists, requestlogin
from peewee import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *

db = SqliteDatabase('ecg.sqlite')

class users(Model):
    username = CharField()
    password = CharField()
    
    class Meta:
        database = db

class Window():
	def __init__(self):
		#Janela
		self.xstart = 300
		self.action = 0
		self.window = QDialog()
		self.window.resize(500, 300)
		self.window.setWindowTitle('Tela de Login')
		self.window.finished.connect(self.endapp)
		#Titulo
		self.title = QLabel('Tela de Login', self.window)
		font = QFont()
		font.setPointSize(20)
		font.setBold(True)
		self.title.setFont(font)
		self.title.move(170, 30)
		#Usuario
		self.nametitle = QLabel('Usuario', self.window)
		self.name = QLineEdit(self.window)
		self.name.resize(200, 30)
		self.nametitle.move(160, 90)
		self.name.move(160, 110)
		#Senha
		self.passwordtitle = QLabel('Senha', self.window)
		self.password = QLineEdit(self.window)
		self.password.setEchoMode(QLineEdit.Password)
		self.password.resize(200, 30)
		self.passwordtitle.move(160, 150)
		self.password.move(160, 170)
		#Login Button
		self.loginbutton = QPushButton('Login', self.window)
		self.loginbutton.move(220, 230)
		self.loginbutton.clicked.connect(self.login)
	
	def login(self, event):
		if self.name.text() and self.password.text():
			if(userexists(self.name.text())):
				if(requestlogin(self.name.text(), self.password.text())):
					self.dlg = QMessageBox(self.window)
					self.dlg.setWindowTitle("Aviso")
					self.dlg.setIcon(QMessageBox.Question)
					self.dlg.setText("Bem-vindo, " + self.name.text())
					self.dlg.exec_()
					self.action = 1
					app.exit()
				else:
					self.dlg = QMessageBox(self.window)
					self.dlg.setWindowTitle("Aviso")
					self.dlg.setIcon(QMessageBox.Question)
					self.dlg.setText("Senha incorreta")
					self.dlg.exec_()
			else:
				self.dlg = QMessageBox(self.window)
				self.dlg.setWindowTitle("Aviso")
				self.dlg.setIcon(QMessageBox.Question)
				self.dlg.setText("Usuario inexistente")
				self.dlg.exec_()
		else:
			self.dlg = QMessageBox(self.window)
			self.dlg.setWindowTitle("Aviso")
			self.dlg.setIcon(QMessageBox.Question)
			self.dlg.setText("Campo usuario ou senha em branco")
			self.dlg.exec_()
	
	def endapp(self, event):
		app.exit()
				
	
	
		
if __name__ == '__main__':
	app = QApplication(sys.argv)
	main = Window()
	main.window.show()
	app.exec_()
	main.window.close()
	if main.action == 1:
		subprocess.Popen('python selection.py', shell=True)
	sys.exit()
