import sys
from os.path import dirname, realpath
import struct
import matplotlib
import subprocess
matplotlib.use('Qt4Agg')
from ecgartools import *
import matplotlib.pyplot as plt
from PyQt4.QtGui import *
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.widgets import Cursor
dir_path = dirname(dirname(dirname(realpath(__file__))))
sys.path.append(dir_path)
from database import savepacient

def RMS(vector):
	ms = 0
	N = len(vector)
	for i in range(0, N):
		ms += vector[i] * vector[i]
	ms /= N
	return ms ** 0.5

class Window():
	def __init__(self):
		#Janela
		self.window = QDialog()
		self.window.resize(1360, 768)
		self.window.setWindowTitle('ECGAR - Late Potentials Analysis')
		
		#Title
		self.title = QLabel('ECGAR - Late Potentials Analysis', self.window)
		font = QFont()
		font.setPointSize(40)
		font.setBold(True)
		self.title.setFont(font)
		self.title.move(300, 10)
		
		#Back Button
		self.backbutton = QPushButton('Voltar', self.window)
		self.backbutton.move(50, 25)
		self.backbutton.clicked.connect(self.backbuttonclick)
		
		#Select Button
		self.selectbutton = QPushButton('Selecionar QRS', self.window)
		self.selectbutton.move(560, 690)
		self.selectbutton.resize(120, 40)
		self.selectbutton.clicked.connect(self.selectbuttonclick)
		
		#Save Button
		self.savebutton = QPushButton('Salvar', self.window)
		self.savebutton.move(720, 690)
		self.savebutton.resize(120, 40)
		self.savebutton.clicked.connect(self.savebuttonclick)
		
		#Variaveis
		self.canvasevent = 0
		self.screen = 0
		self.cursor = None
		self.graph = None
		self.firstcursor = None
		self.secondcursor = None
		self.motevent = None
		self.lasttext = None
		f = open("temp/data2.dat", "rb")
		self.VM = None
		self.signalinput = [[], [], []]
		while True:
			
			x = f.read(8)
			if x == '':
				break
			self.signalinput[0].append(struct.unpack('d', x)[0])
			for i in range(1, 3):
				self.signalinput[i].append(struct.unpack('d', f.read(8))[0])
			if x == '':
				break
		
		self.VM = vector_magnitude(self.signalinput[0], self.signalinput[1], self.signalinput[2])
				
		self.firstclick = 1
		
		
		#Canvas
		plt.style.use('ggplot')
		self.figure = plt.figure()
		self.canvas = FigureCanvas(self.figure)
		self.canvas.resize(1360, 600)
		self.canvas.setParent(self.window)
		self.canvas.move(0, 80)
		self.canvas.mpl_connect('button_press_event', self.canvasclick)
		self.canvas.draw()
		self.plot()
	
	def on_plot_hover(self, event):
		if event.xdata != None:
			i1 = min(int(self.firstcursor.get_xdata()[0]), int(event.xdata))
			i2 = max(int(self.firstcursor.get_xdata()[0]), int(event.xdata))
			qrsd = i2-i1
			print i1, i2
			rms40 = RMS(self.VM[max(i1, i2-40):i2])
			ran = range(i1, i2)[::-1]
			for i in ran:
				if self.VM[i] > 40:
					las40 = i2-i
					break
			else:
				las40 = i2-i1
			if self.lasttext:
				self.lasttext.remove()
			self.lasttext = plt.text(900, 60, 'QRSD = %.1f ms\nRMS40 = %.2f uV\nLAS40 = %.1f ms' % (qrsd, rms40, las40), fontdict=None, color = 'k')
	
	def plot(self):
		self.graph = self.figure.add_subplot(111)
		self.graph.plot(self.VM, color='red')
		self.graph.grid(color='orange', linestyle = '-', linewidth = 1)
		self.cursor = Cursor(self.graph, horizOn = 0, useblit = False, color='blue', linewidth = 2)
		self.cursor.set_active(False)
		self.canvas.draw()
	
	def canvasclick(self, event):
		print event.xdata, event.ydata
		if self.canvasevent == 1:
			if event.xdata != None:
				if self.firstclick:
					self.firstcursor = self.graph.axvline(x=event.xdata, linewidth=2, color = 'green')
					self.canvas.draw()
					self.firstclick = 0
					self.motevent = self.canvas.mpl_connect('motion_notify_event', self.on_plot_hover) 
				else:
					self.secondcursor = self.graph.axvline(x=event.xdata, linewidth=2, color = 'blue')
					self.canvas.draw()
					self.cursor.set_active(False)
					self.canvas.mpl_disconnect(self.motevent) 
					self.firstclick = 1
					self.canvasevent = 0
					self.unlockall()
		
	def lockall(self):
		pass
		
	def unlockall(self):
		pass
	
	def backbuttonclick(self, event):
		self.screen = 1
		app.exit()
	
	def selectbuttonclick(self, event):
		#print event.xdata, event.ydata
		self.cursor.set_active(True)
		self.canvasevent = 1
		if self.lasttext:
			self.lasttext.remove()
			self.lasttext = None
		if self.firstcursor != None:
			self.firstcursor.remove()
			self.firstcursor = None
		if self.secondcursor != None:
			self.secondcursor.remove()
			self.secondcursor = None
		self.lockall()
	
	def savebuttonclick(self, event):
		self.screen = 2
		savepacient()
		app.exit()
		



app = QApplication(sys.argv)
main = Window()
main.window.show()
app.exec_()
if main.screen == 1:
	subprocess.Popen('python exams/ECGAR/ecgar.py', shell = True)
if main.screen == 2:
	subprocess.Popen('python selection.py', shell = True)
sys.exit()
