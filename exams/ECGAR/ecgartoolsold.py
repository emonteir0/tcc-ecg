__author__ = 'marc_vinici'

import numpy as np
import scipy.signal as scisig
import wfdb


class ECGAR(object):

    def __init__(self):
        pass

    def read_files(self, path, type_file):
        pass

    def convert_to_xyz(self, type_name, sig):
        x = []
        y = []
        z = []
        signals = np.array(sig)
        if type_name == 'Dower':

            x = (-0.172*signals[0] - 0.073*signals[1] + 0.122*signals[2] + 0.231*signals[3] + 0.239*signals[4]
                 + 0.193*signals[5] + 0.156*signals[6] - 0.009*signals[7])

            y = (0.057*signals[0] - 0.019*signals[1] - 0.106*signals[2] - 0.022*signals[3] + 0.040*signals[4]
                 + 0.048*signals[5] - 0.227*signals[6] - 0.886*signals[7])

            z = (-0.228*signals[0] - 0.310*signals[1] - 0.245*signals[2] - 0.063*signals[3] + 0.054*signals[4]
                 + 0.108*signals[5] + 0.021*signals[6] + 0.102*signals[7])

        elif type_name == 'PLSV':

            x = (-0.172*signals[0] - 0.073*signals[1] + 0.122*signals[2] + 0.231*signals[3] + 0.239*signals[4]
                 + 0.193*signals[5] + 0.156*signals[6] - 0.009*signals[7])

            y = (0.057*signals[0] - 0.019*signals[1] - 0.106*signals[2] - 0.022*signals[3] + 0.040*signals[4]
                 + 0.048*signals[5] - 0.227*signals[6] - 0.886*signals[7])

            z = (-0.228*signals[0] - 0.310*signals[1] - 0.245*signals[2] - 0.063*signals[3] + 0.054*signals[4]
                 + 0.108*signals[5] + 0.021*signals[6] + 0.102*signals[7])

        elif type_name == 'QLSV':

            x = (-0.172*signals[0] - 0.073*signals[1] + 0.122*signals[2] + 0.231*signals[3] + 0.239*signals[4]
                 + 0.193*signals[5] + 0.156*signals[6] - 0.009*signals[7])

            y = (0.057*signals[0] - 0.019*signals[1] - 0.106*signals[2] - 0.022*signals[3] + 0.040*signals[4]
                 + 0.048*signals[5] - 0.227*signals[6] - 0.886*signals[7])

            z = (-0.228*signals[0] - 0.310*signals[1] - 0.245*signals[2] - 0.063*signals[3] + 0.054*signals[4]
                 + 0.108*signals[5] + 0.021*signals[6] + 0.102*signals[7])

        converted_signals = [x.tolist(), y.tolist(), z.tolist()]

        return converted_signals

    """ param = [fsamp, delta, Rp, Rs]  """
    def remove_base_line_drift(self, param, sig):
        print 'base-line'
        fsamp = float(param[0])
        delta = float(param[1])
        Rp = float(param[2])
        Rs = float(param[3])
        fcuts = [.67-delta, .67+delta]
        nhp, Wnhp = scisig.cheb2ord((2.0*fcuts[1])/fsamp, (2.0*fcuts[0])/fsamp, Rp, Rs)
        b1, a1 = scisig.cheby2(nhp, Rs, Wnhp, 'high')
        x = scisig.lfilter(b1, a1, sig[0][::-1], axis=0)
        y = scisig.lfilter(b1, a1, sig[1][::-1], axis=0)
        z = scisig.lfilter(b1, a1, sig[2][::-1], axis=0)

        x = scisig.lfilter(b1, a1, x[::-1], axis=0)
        y = scisig.lfilter(b1, a1, y[::-1], axis=0)
        z = scisig.lfilter(b1, a1, z[::-1], axis=0)


        base_line_drift_signals = [x.tolist(), y.tolist(), z.tolist()]

        return base_line_drift_signals

    """ param = [fsamp, delta, Rp, Rs]  """
    def remove_high_frequency_noise(self, param, sig):

        fsamp = param[0]
        delta = param[1]
        Rp = param[2]
        Rs = param[3]
        fcuts = [430-delta, 160+delta]
        nlp, Wnlp = scisig.cheb2ord((2.0*fcuts[0])/fsamp, (2.0*fcuts[1])/fsamp, Rp, Rs)
        b2, a2 = scisig.cheby2(nlp, Rs, Wnlp, 'low')

        x = scisig.lfilter(b2, a2, sig[0][::-1], axis=0)
        y = scisig.lfilter(b2, a2, sig[1][::-1], axis=0)
        z = scisig.lfilter(b2, a2, sig[2][::-1], axis=0)

        x = scisig.lfilter(b2, a2, x[::-1], axis=0)
        y = scisig.lfilter(b2, a2, y[::-1], axis=0)
        z = scisig.lfilter(b2, a2, z[::-1], axis=0)

        remove_high_frequency_noise_signals = [x.tolist(), y.tolist(), z.tolist()]

        return remove_high_frequency_noise_signals

    def cross_correlation(self, sig, fs=1000.0, adcgain=1000.0, adczero=0):

        peaks_indexes = wfdb.processing.gqrs_detect(x=sig[0], fs=fs, adcgain=adcgain, adczero=adczero, threshold=1.0)
        print peaks_indexes[0]
        min_bpm = 10
        max_bpm = 350
        min_gap = fs*60/min_bpm
        max_gap = fs*60/max_bpm
        new_indexes = wfdb.processing.correct_peaks(x=sig[0], peak_indices=peaks_indexes, min_gap=min_gap, max_gap=max_gap, smooth_window=150)
        tamplate_x = sig[0][new_indexes[0]-500:new_indexes[0]+500]
        tamplate_y = sig[0][new_indexes[0]-500:new_indexes[0]+500]
        tamplate_z = sig[0][new_indexes[0]-500:new_indexes[0]+500]
        t1 = int(new_indexes[0]-500)
        t2 = int(new_indexes[0]+500)
        #t1 = int(200)
        #t2 = int(1199)
        x = sig[0][t1:t2]
        w = np.correlate(sig[0], tamplate_x)
        print len(w), len(sig[0])
        t = np.linspace(0, len(x)-1, len(x))
        print x.shape

        return w

    """ param = [trhesh_const]  """
    def mean_signals(self, threshold, sig, sig_cor):

        w = sig_cor
        thresh_const = float(threshold)
        index = []
        thresh = thresh_const*(w[0:5000].max())

        start = 1

        for i in range(start, len(w)-1):
            if ((w[i] >  thresh) and (w[i] >  w[i-1]) and  (w[i] >  w[i+1])):
                index.append(i)

        lint = 1000
        ave1 = np.zeros(lint)
        ave2 = np.zeros(lint)
        ave3 = np.zeros(lint)
        counter = 0
        print index
        for i in range(1, len(index)):
            k = index[i]
            if k+lint <= 1000000:
                ave1 += sig[0][k:k+lint]
                ave2 += sig[1][k:k+lint]
                ave3 += sig[2][k:k+lint]
                counter += 1

        ave1 = ave1/counter
        ave2 = ave2/counter
        ave3 = ave3/counter

        print counter

        sig1 = ave1
        sig2 = ave2
        sig3 = ave3

        mean_signals = [sig1, sig2, sig3]
        return mean_signals

    """ param = [fsamp, order, fcut1, fcut2, type_filter, fiducial_point]  """
    def butterworth(self, param, sig):

        fsamp = float(param[0])
        order = float(param[1])
        fcut1 = float(param[2])
        fcut2 = float(param[3])
        type_filter = str(param[4])
        fiducial_point = int(param[5])

        B, A = scisig.butter(order, [fcut1/(fsamp/2), fcut2/(fsamp/2)], type_filter)

        b1 = scisig.lfilter(B,A,sig[0][:fiducial_point:-1])
        b2 = scisig.lfilter(B,A,sig[1][:fiducial_point:-1])
        b3 = scisig.lfilter(B,A,sig[2][:fiducial_point:-1])

        b1 = b1[::-1]
        b2 = b2[::-1]
        b3 = b3[::-1]

        b11 =  scisig.lfilter(B,A,sig[0][0:fiducial_point])
        b22 =  scisig.lfilter(B,A,sig[0][0:fiducial_point])
        b33 =  scisig.lfilter(B,A,sig[0][0:fiducial_point])

        c1 = np.hstack((b11,b1))
        c2 = np.hstack((b22,b2))
        c3 = np.hstack((b33,b3))

        butterworth_signals = [c1, c2, c3]

        return butterworth_signals

    def vector_magnitude(self, sig):

        print 'VM'
        VM = np.sqrt(np.square((sig[0]))+np.square((sig[1]))+np.square((sig[2])))

        return VM

