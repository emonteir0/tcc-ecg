import csv
import sys
import struct
import time
import subprocess
from PyQt4.QtGui import *
from PyQt4.QtCore import *

import matplotlib
matplotlib.use('Qt4Agg')
from ecgartools import ECGAR

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from matplotlib.widgets import MultiCursor


class Window():
	def __init__(self):
		#Janela
		self.window = QDialog()
		self.window.resize(1360, 768)
		self.window.setWindowTitle('ECGAR: Pre-processamento')
		#Titulo
		self.title = QLabel('ECGAR: Pre-processamento', self.window)
		font = QFont()
		font.setPointSize(40)
		font.setBold(True)
		self.title.setFont(font)
		self.title.move(320, 20)
		#Variaveis
		self.canvasevent = 0
		self.index = 0
		self.cursor = None
		self.cursor2 = None
		self.graphs = [None, None, None]
		self.firstcursors = [None, None, None]
		self.secondcursors = [None, None, None]
		self.tools = ECGAR()
		f = open("temp/data.dat", "rb")
		self.data = None
		self.signalinput = [[], [], [], [], [], [], [], []]
		data = [0, 0, 0, 0, 0, 0, 0, 0]
		while True:
			
			x = f.read(8)
			if x == '':
				break
			self.signalinput[0].append(struct.unpack('d', x)[0])
			for i in range(1, 8):
				self.signalinput[i].append(struct.unpack('d', f.read(8))[0])
			if x == '':
				break
		self.data = self.tools.remove_high_frequency_noise([1000, 140, .05, 40], self.tools.remove_base_line_drift([1000, .5, .05, 40], self.tools.convert_to_xyz('Dower', self.signalinput)))
		self.firstclick = 1
		
		#Canvas
		plt.style.use('ggplot')
		self.figure = plt.figure()
		self.canvas = FigureCanvas(self.figure)
		self.canvas.resize(1360, 550)
		self.canvas.setParent(self.window)
		self.canvas.move(0, 130)
		self.canvas.mpl_connect('button_press_event', self.canvasclick)
		self.plot()
		
		#Select Button
		self.selectbutton = QPushButton('Selecionar Dados', self.window)
		self.selectbutton.move(400, 90)
		self.selectbutton.clicked.connect(self.selectbuttonclick)
		
		#Remove Button
		self.removebutton = QPushButton('Remover Dados', self.window)
		self.removebutton.move(800, 90)
		self.removebutton.clicked.connect(self.removebuttonclick)
		
		
		#Pagina anterior
		self.previousbutton = QPushButton(self.window)
		self.previousbutton.setIcon(QIcon('images/back.png'))
		self.previousbutton.move(20, 360)
		self.previousbutton.setIconSize(QSize(40, 40))
		self.previousbutton.clicked.connect(self.previousevent)
		
		#Proxima pagina
		self.nextbutton = QPushButton(self.window)
		self.nextbutton.setIcon(QIcon('images/forward.png'))
		self.nextbutton.setIconSize(QSize(40, 40))
		self.nextbutton.move(1280, 360)
		self.nextbutton.clicked.connect(self.nextevent)
		
		#Avancar
		self.forwardbutton = QPushButton('Avancar', self.window)
		self.forwardbutton.move(1200, 50)
		self.forwardbutton.clicked.connect(self.forwardevent)
	
	def forwardevent(self, event):
		self.action = 2
		out_file = open('temp/data2.dat',"wb")
		for i in range(0, len(self.data[0])):
			s = struct.pack('ddd', self.data[0][i], self.data[1][i], self.data[2][i])
			out_file.write(s)
		out_file.close()
		app.exit()
	
	def previousevent(self, event):
		if self.index > 0:
			self.index -= 1
			self.plot()
		print self.index
		
	def nextevent(self, event):
		if self.index < len(self.data[0])/5000 - 1:
			self.index += 1
			self.plot()
		print self.index
	
	def plot(self):
		for i in range(0, 3):
			if self.graphs[i]:
				self.graphs[i].remove()
			self.graphs[i] = self.figure.add_subplot(311+i)
			sz = len(self.data[i][self.index*5000 : self.index*5000 + 10000])
			self.graphs[i].plot(range(self.index*5000, self.index*5000 + sz), self.data[i][self.index*5000 : self.index*5000 + sz], color='red')
			self.graphs[i].grid(color='orange', linestyle = '-', linewidth = 1)
		self.cursor = MultiCursor(self.canvas, (self.graphs[0], self.graphs[1], self.graphs[2]), useblit = False, color='blue', linewidth = 2)
		self.cursor.set_active(False)
		self.canvas.draw()
	
	def canvasclick(self, event):
		print event.xdata, event.ydata
		if self.canvasevent == 1:
			if event.xdata != None:
				if self.firstclick:
					for i in range(0, 3):
						self.firstcursors[i] = self.graphs[i].axvline(x=event.xdata, linewidth=2, color = 'green')
					
					self.canvas.draw()
					self.cursor.color = 'green'
					self.firstclick = 0
				else:
					for i in range(0, 3):
						self.secondcursors[i] = self.graphs[i].axvline(x=event.xdata, linewidth=2, color = 'blue')
					
					self.canvas.draw()
					self.cursor.set_active(False)
					self.firstclick = 1
					self.canvasevent = 0
					self.cursor.color = 'red'
					self.unlockall()
		
	def lockall(self):
		pass
		
	def unlockall(self):
		pass
	
	def removebuttonclick(self, event):
		if self.firstcursors[0] and self.secondcursors[0]:
			i1 = min(int(self.firstcursors[0].get_xdata()[0]), int(self.secondcursors[0].get_xdata()[0]))
			i2 = max(int(self.firstcursors[0].get_xdata()[0]), int(self.secondcursors[0].get_xdata()[0]))
			print i1, i2
			for i in range(0, 3):
				print type(self.data[i])
				self.data[i] = self.data[i][0:i1] + self.data[i][i2+1:]
				self.firstcursors[i].remove()
				self.firstcursors[i] = None
				self.secondcursors[i].remove()
				self.secondcursors[i] = None
			print len(self.data[0])
			if self.index >= len(self.data[0])/10000:
				self.index -= 1
			self.plot()
	
	def selectbuttonclick(self, event):
		#print event.xdata, event.ydata
		self.cursor.set_active(True)
		self.cursor.color = 'red'
		self.canvasevent = 1
		for i in range(0, 3):
			if self.firstcursors[i] != None:
				self.firstcursors[i].remove()
			if self.secondcursors[i] != None:
				self.secondcursors[i].remove()
		self.lockall()
        


if __name__ == '__main__':
	
	app = QApplication(sys.argv)
	main = Window()
	main.window.show()
	app.exec_()
	subprocess.Popen('python exams/ECGAR/magnitude.py', shell = True)
	sys.exit()
