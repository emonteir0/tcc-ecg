import matplotlib.pyplot as plt
import numpy as np
import math as math
import sys
import struct

VM = []
params = []

for param in sys.argv:
	params.append(param)

	
print params[6]
	
x1 = int(params[1]) + 200
x2 = int(params[2]) + 200
qrsd = float(params[3])
rms40 = float(params[4])
las40 = float(params[5])	
f = open(params[6] + "test.dat", "rb")
	
while True:
	value = f.read(8)
	if value == '':
		break
	VM.append(struct.unpack('d', value)[0])

x = range(0,600)
plt.plot(x, VM[200:800])
x0 = x2 - 40
y = (np.zeros((1,x0))).tolist()[0] + VM[x0:x2+1] + (np.zeros((1,999-x2))).tolist()[0]
print(len(y))
plt.fill_between(x,0,y[200:800])
plt.hold()
plt.axvline(x1-200,linewidth=2, color = 'k', ymin = 0, ymax = 0.5625)
plt.hold()
plt.axvline(x2-200,linewidth=2, color = 'k', ymin = 0, ymax = 0.5625)
plt.text(450, 60, 'QRSD = %.1f ms\nRMS40 = %.2f uV\nLAS40 = %.1f ms' % (qrsd, rms40, las40), fontdict=None, color = 'k')
plt.title('Late potentials graph')
plt.xlabel('n')
plt.ylabel('Magnitude (uV)')
plt.grid(which='both', axis='both')
if dir == '\VAZIO/':
	plt.savefig('imagem.png')
else:
	plt.savefig('imagem.png')
plt.show()