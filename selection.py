import sys
import json
import struct
import time
import commands
import subprocess
from PyQt4.QtGui import *
from PyQt4.QtCore import *

	

class Window():
	def __init__(self):
		#Janela
		self.xstart = 300
		self.window = QDialog()
		self.window.resize(500, 300)
		self.window.setWindowTitle('Tela de Exames')
		self.window.finished.connect(self.endapp)
		self.screen = 0
		#Titulo
		self.title = QLabel('Tela de Exames', self.window)
		font = QFont()
		font.setPointSize(20)
		font.setBold(True)
		self.title.setFont(font)
		self.title.move(160, 30)
		#Novo Exame
		self.newexams = QPushButton('Novo Exame', self.window)
		self.newexams.move(185, 80)
		self.newexams.resize(150, 50)
		self.newexams.clicked.connect(self.newexam)
		#Exames Cadastrados
		self.olderexams = QPushButton('Exames Cadastrados', self.window)
		self.olderexams.move(185, 150)
		self.olderexams.resize(150, 50)
		self.olderexams.clicked.connect(self.olderexam)
		#Logout Button
		self.logoutbutton = QPushButton('Logout', self.window)
		self.logoutbutton.move(185, 220)
		self.logoutbutton.resize(150, 50)
		self.logoutbutton.clicked.connect(self.logout)
	
	def newexam(self, event):
		self.screen = 1
		app.exit()
		
	def olderexam(self, event):
		self.screen = 2
		app.exit()
	
	def logout(self, event):
		self.screen = 3
		app.exit()
	
	def endapp(self, event):
		app.exit()
				
	
	
		
if __name__ == '__main__':
	app = QApplication(sys.argv)
	main = Window()
	main.window.show()
	app.exec_()
	main.window.close()
	if main.screen == 1:
		subprocess.Popen('python main.py', shell=True)
	elif main.screen == 2:
		subprocess.Popen('python olderexam.py', shell=True)
	elif main.screen == 3:
		subprocess.Popen('python login.py', shell=True)
	sys.exit()
