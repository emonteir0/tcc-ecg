import RPi.GPIO as GPIO
from periphery import SPI
import time
import matplotlib.pyplot as plt

PIN_RESET = 40
PIN_START = 37
PIN_DRDY = 38
PIN_CS = 36

def func(arr):
    arr2 = []
    for x in arr:
        arr2.append(hex(x))
    return arr2

"""def adc_send_command(spi, cmd):
    spi.transfer([cmd])

def adc_wreg(spi, reg, val):
    spi.transfer([0x40+reg, 0, val])

def adc_rreg(spi, reg):
    out = 0
    out = spi.transfer([0x20+reg, 0, 0])[2]
    return [out]"""

def adc_send_command(spi, cmd):
    GPIO.output(PIN_CS, 0)
    spi.transfer([cmd])
    time.sleep(2e-6)
    GPIO.output(PIN_CS, 1)
    time.sleep(2e-6)

def adc_wreg(spi, reg, val):
    GPIO.output(PIN_CS, 0)
    spi.transfer([0x40+reg])
    spi.transfer([0])
    out = spi.transfer([val])
    time.sleep(2e-6)
    GPIO.output(PIN_CS, 1)
    

def adc_rreg(spi, reg):
    out = 0
    GPIO.output(PIN_CS, 0)
    spi.transfer([0x20+reg])
    spi.transfer([0])
    time.sleep(1e-6)
    out = spi.transfer([0])
    time.sleep(2e-6)
    GPIO.output(PIN_CS, 1)
    return out

def getData(spi):
    a = spi.transfer([0x00])[0]
    b = spi.transfer([0x00])[0]
    c = spi.transfer([0x00])[0]
    return (a << 16) + (b << 8) + c
    
GPIO.setmode(GPIO.BOARD)
GPIO.setup(PIN_START, GPIO.OUT)
GPIO.setup(PIN_RESET, GPIO.OUT)
GPIO.setup(PIN_CS, GPIO.OUT)
#GPIO.setup(PIN_DRDY, GPIO.IN)
GPIO.setup(PIN_DRDY, GPIO.IN, pull_up_down = GPIO.PUD_UP)
#GPIO.output(PIN_START, 0)
GPIO.output(PIN_CS, 1)
GPIO.output(PIN_RESET, 0)
time.sleep(1)
GPIO.output(PIN_RESET, 1)
spi = SPI("/dev/spidev0.0", 0, 400000)
spi.mode = 1
time.sleep(1)

GPIO.output(PIN_START, 0)

#adc_send_command(spi, 0x11)
adc_send_command(spi, 0x12)
print func(adc_rreg(spi, 0x00))
#print spi.transfer([0x11])
#spi.transfer([0x12])
time.sleep(0.1)
######################SET_REGISTERS##############################33
# CONFIG1
"""spi.transfer([0x41, 0x00, 0x85])
print func(spi.transfer([0x21, 0x00, 0x00]))"""
adc_wreg(spi, 0x01, 0x84)
print func(adc_rreg(spi, 0x01))
# CONFIG2
"""spi.transfer([0x42, 0x00, 0x10])
print func(spi.transfer([0x22, 0x00, 0x00]))"""
adc_wreg(spi, 0x02, 0x00)
print func(adc_rreg(spi, 0x02))
# CONFIG3
"""spi.transfer([0x43, 0x00, 0xDC])
print func(spi.transfer([0x23, 0x00, 0x00]))"""
adc_wreg(spi, 0x03, 0xCC)
print func(adc_rreg(spi, 0x03))
# LOFF
"""spi.transfer([0x44, 0x00, 0x03])
print func(spi.transfer([0x24, 0x00, 0x00]))"""
adc_wreg(spi, 0x04, 0x13)
print func(adc_rreg(spi, 0x04))
# CH1SET
"""spi.transfer([0x45, 0x00, 0x60])
print func(spi.transfer([0x25, 0x00, 0x00]))"""
adc_wreg(spi, 0x05, 0x00)
print func(adc_rreg(spi, 0x05))
# CH2SET
"""spi.transfer([0x46, 0x00, 0x60])
print func(spi.transfer([0x26, 0x00, 0x00]))"""
adc_wreg(spi, 0x06, 0x00)
print func(adc_rreg(spi, 0x06))
# CH3SET
"""spi.transfer([0x47, 0x00, 0x60])
print func(spi.transfer([0x27, 0x00, 0x00]))"""
adc_wreg(spi, 0x07, 0x00)
print func(adc_rreg(spi, 0x07))
# CH4SET
"""spi.transfer([0x48, 0x00, 0x60])
print func(spi.transfer([0x28, 0x00, 0x00]))"""
adc_wreg(spi, 0x08, 0x00)
print func(adc_rreg(spi, 0x08))
# CH5SET
"""spi.transfer([0x49, 0x00, 0x60])
print func(spi.transfer([0x29, 0x00, 0x00]))"""
adc_wreg(spi, 0x09, 0x00)
print func(adc_rreg(spi, 0x09))
# CH6SET
"""spi.transfer([0x4A, 0x00, 0x60])
print func(spi.transfer([0x2A, 0x00, 0x00]))"""
adc_wreg(spi, 0x0A, 0x00)
print func(adc_rreg(spi, 0x0A))
# CH7SET
"""spi.transfer([0x4B, 0x00, 0x60])
print func(spi.transfer([0x2B, 0x00, 0x00]))"""
adc_wreg(spi, 0x0B, 0x00)
print func(adc_rreg(spi, 0x0B))
# CH8SET
"""spi.transfer([0x4C, 0x00, 0x60])
print func(spi.transfer([0x2C, 0x00, 0x00]))"""
adc_wreg(spi, 0x0C, 0x00)
print func(adc_rreg(spi, 0x0C))
# RLD_SENSP
"""spi.transfer([0x4D, 0x00, 0x00])
print func(spi.transfer([0x2D, 0x00, 0x00]))"""
adc_wreg(spi, 0x0D, 0x06)
print func(adc_rreg(spi, 0x0D))
# RLD_SENSN
"""spi.transfer([0x4E, 0x00, 0x00])
print func(spi.transfer([0x2E, 0x00, 0x00]))"""
adc_wreg(spi, 0x0E, 0x02)
print func(adc_rreg(spi, 0x0E))
# LOFF_SENSP
"""spi.transfer([0x4F, 0x00, 0xFF])
print func(spi.transfer([0x2F, 0x00, 0x00]))"""
adc_wreg(spi, 0x0F, 0xFF)
print func(adc_rreg(spi, 0x0F))
# LOFF_SENSN
"""spi.transfer([0x50, 0x00, 0x02])
print func(spi.transfer([0x30, 0x00, 0x00]))"""
adc_wreg(spi, 0x10, 0x02)
print func(adc_rreg(spi, 0x10))
# LOFF_FLIP
"""spi.transfer([0x51, 0x00, 0x00])
print func(spi.transfer([0x31, 0x00, 0x00]))"""
adc_wreg(spi, 0x11, 0x00)
print func(adc_rreg(spi, 0x11))
# GPIO
"""spi.transfer([0x54, 0x00, 0x00])
print func(spi.transfer([0x34, 0x00, 0x00]))"""
adc_wreg(spi, 0x14, 0x0F)
print func(adc_rreg(spi, 0x14))
# PACE
"""spi.transfer([0x55, 0x00, 0x00])
print func(spi.transfer([0x35, 0x00, 0x00]))"""
adc_wreg(spi, 0x15, 0x00)
print func(adc_rreg(spi, 0x15))
# RESP
"""spi.transfer([0x56, 0x00, 0xF0])
print func(spi.transfer([0x36, 0x00, 0x00]))"""
adc_wreg(spi, 0x16, 0x00)
print func(adc_rreg(spi, 0x16))
#CONFIG 4
"""spi.transfer([0x57, 0x00, 0x22])
print func(spi.transfer([0x37, 0x00, 0x00]))"""
adc_wreg(spi, 0x17, 0x02)
print func(adc_rreg(spi, 0x17))
#WCT1
"""spi.transfer([0x58, 0x00, 0x0A])
print func(spi.transfer([0x38, 0x00, 0x00]))"""
adc_wreg(spi, 0x18, 0x0A)
print func(adc_rreg(spi, 0x18))
#WCT2
"""spi.transfer([0x59, 0x00, 0xE3])
print func(spi.transfer([0x39, 0x00, 0x00]))"""
adc_wreg(spi, 0x19, 0xE3)
print func(adc_rreg(spi, 0x19))

#GPIO.output(PIN_START, 1)
########################COMUNICACAO################
while True:
    v = [[], [], [], [], [], [], [], []]
    
    #GPIO.output(PIN_START, 1)
    #spi.transfer([0x08])
    #spi.transfer([0x10])
    adc_send_command(spi, 0x08)
    GPIO.output(PIN_CS, 0)
    adc_send_command(spi, 0x10)
    time.sleep(25e-6)
    GPIO.output(PIN_CS, 1)
    N = 0
    while N < 2000:
        if GPIO.input(PIN_DRDY) == 0:
            GPIO.output(PIN_CS, 0)
            getData(spi)
            for j in range(0, 8):
                v[j].append(getData(spi))
            #time.sleep(1e-6)
            #GPIO.output(PIN_CS, 1)
            time.sleep(25e-6)
            N += 1
    adc_send_command(spi, 0x0A)
    #spi.transfer([0x0A])
    #GPIO.output(PIN_START, 0) 
    f, arr = plt.subplots(4,2)
    arr[0, 0].plot(v[0])
    arr[0, 1].plot(v[1])
    arr[1, 0].plot(v[2])
    arr[1, 1].plot(v[3])
    arr[2, 0].plot(v[4])
    arr[2, 1].plot(v[5])
    arr[3, 0].plot(v[6])
    arr[3, 1].plot(v[7])
    plt.show()
    print "CULTURA"
    time.sleep(1)

spi.close()
GPIO.cleanup()
