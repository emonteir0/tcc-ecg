import struct
import numpy as np
from ecgartools2 import *
import matplotlib.pyplot as plt

"""f = open('


while True:		
	x = f.read(8)
	if x == '':
		break
	self.signalinput[0].append(struct.unpack('d', x)[0])
	for i in range(1, 8):
		self.signalinput[i].append(struct.unpack('d', f.read(8))[0])
	if x == '':
		break"""
		
fx = open('X.dat')
fy = open('Y.dat')
fz = open('Z.dat')

X = []
Y = []
Z = []

while True:
	x = fx.read(8)
	if x == '':
		break
	X.append(struct.unpack('d', x)[0])
	Y.append(struct.unpack('d', fy.read(8))[0])
	Z.append(struct.unpack('d', fz.read(8))[0])



X = np.array(X)*1000*1000
Y = np.array(Y)*1000*1000
Z = np.array(Z)*1000*1000


[y1, y2, y3] = baselinedrift(X, Y, Z)
[z1, z2, z3] = remove_high_frequency_noise(y1, y2, y3)
vc = cross_correlation(z1, z2, z3)
plt.plot(vc)
plt.show()
