import numpy as np
import scipy.signal as scisig
import wfdb

fsamp = 1000.

"""def convert_to_xyz(sig):
        signals = np.array(sig)

		x = (-0.172*signals[0] - 0.073*signals[1] + 0.122*signals[2] + 0.231*signals[3] + 0.239*signals[4] + 0.193*signals[5] + 0.156*signals[6] - 0.009*signals[7])

		y = (0.057*signals[0] - 0.019*signals[1] - 0.106*signals[2] - 0.022*signals[3] + 0.040*signals[4] + 0.048*signals[5] - 0.227*signals[6] - 0.886*signals[7])

		z = (-0.228*signals[0] - 0.310*signals[1] - 0.245*signals[2] - 0.063*signals[3] + 0.054*signals[4] + 0.108*signals[5] + 0.021*signals[6] + 0.102*signals[7])

        converted_signals = [x.tolist(), y.tolist(), z.tolist()]

        return converted_signals
"""

def baselinedrift(X, Y, Z):
	delta =  .5

	fcuts =  [.67-delta,.67+delta]
	Rp  =  .05
	Rs  =  40

	nhp, Wnhp =  scisig.cheb2ord((2.0*fcuts[1])/fsamp,(2.0*fcuts[0])/fsamp,Rp,Rs)
	b1, a1 =  scisig.cheby2(nhp,Rs,Wnhp,'high')
	w1, h1 =  scisig.freqz(b1,a1)



	y1 = scisig.lfilter(b1,a1,X[::-1],axis=0)
	y2 = scisig.lfilter(b1,a1,Y[::-1],axis=0)
	y3 = scisig.lfilter(b1,a1,Z[::-1],axis=0)

	y1 = scisig.lfilter(b1,a1,y1[::-1],axis=0)
	y2 = scisig.lfilter(b1,a1,y2[::-1],axis=0)
	y3 = scisig.lfilter(b1,a1,y3[::-1],axis=0)

	return [y1.tolist(), y2.tolist(), y3.tolist()]
	
def remove_high_frequency_noise(y1, y2, y3):
	delta =  140.0

	fcuts =  [160.0-delta,160.0+delta]
	Rp  =  .05
	Rs  =  40

	nlp, Wnlp =  scisig.cheb2ord((2.0*fcuts[0])/fsamp,(2.0*fcuts[1])/fsamp,Rp,Rs)
	b2, a2 =  scisig.cheby2(nlp,Rs,Wnlp,'low')
	h2, w2 =  scisig.freqz(b2,a2)


	z1 =  scisig.lfilter(b2,a2,y1[::-1],axis=0)
	z2 =  scisig.lfilter(b2,a2,y2[::-1],axis=0)
	z3 =  scisig.lfilter(b2,a2,y3[::-1],axis=0)

	z1 =  scisig.lfilter(b2,a2,z1[::-1],axis=0)
	z2 =  scisig.lfilter(b2,a2,z2[::-1],axis=0)
	z3 =  scisig.lfilter(b2,a2,z3[::-1],axis=0)
	
	
	return [z1, z2, z3]
	
def cross_correlation(z1, z2, z3):
	record = wfdb.rdsamp('standards/s0017lre', channels= [12, 13, 14], physical= False)
	peaks_indexes = wfdb.processing.gqrs_detect(x=z1, fs=record.fs*2, adcgain=record.adcgain[0], adczero=record.adczero[0], threshold=1.0)
	
	min_bpm = 10
	max_bpm = 350
	min_gap = record.fs*60/min_bpm
	max_gap = record.fs*60/max_bpm
	new_indexes = wfdb.processing.correct_peaks(x=z1, peak_indices=peaks_indexes, min_gap=min_gap, max_gap=max_gap, smooth_window=150)

	
	tamplate_x = z1[new_indexes[0]-500*int(fsamp/1000):new_indexes[0]+500*int(fsamp/1000)]
	tamplate_y = z2[new_indexes[0]-500*int(fsamp/1000):new_indexes[0]+500*int(fsamp/1000)]
	tamplate_z = z3[new_indexes[0]-500*int(fsamp/1000):new_indexes[0]+500*int(fsamp/1000)]


	t1 = int(new_indexes[3]-500*int(fsamp/1000))
	t2 = int(new_indexes[3]+500*int(fsamp/1000))
	#t1 = int(200)
	#t2 = int(1199)
	x = z1[t1:t2]
	w = np.correlate(z1,tamplate_x)
	#print len(w), len(z1)
	t = np.linspace(0, len(x)-1, len(x))
	#print x.shape, tamplate_x.shape
	print 'cross correlation'

	index = []
	thresh_central = np.mean((w[5000:10000*int(fsamp/1000)].max()))
	thresh_inf = 0.98*thresh_central
	thresh_sup = 1.02*thresh_central

	print thresh_central, thresh_inf, thresh_sup

	start = 1

	for i in range(start,len(w)-1):
		if ((w[i]< thresh_sup) and (w[i] >  thresh_inf) and (w[i] >  w[i-1]) and  (w[i] >  w[i+1])):
			index.append(i)

	lint =  999*int(fsamp/1000)
	print 'mean signals'
	# Sinal combinado
	ave1 =  np.zeros(lint)
	ave2 =  np.zeros(lint)
	ave3 =  np.zeros(lint)
	counter =  0
	for i in range(1,len(index)):
		k=index[i]
		if  k+lint<=1000000:
			ave1 += z1[k:k+int(lint)]
			ave2 += z2[k:k+int(lint)]
			ave3 += z3[k:k+int(lint)]
			counter +=  1

	ave1 =  ave1/counter
	ave2 =  ave2/counter
	ave3 =  ave3/counter
	

	sig1 =  ave1
	sig2 =  ave2
	sig3 =  ave3

	

	B, A = scisig.butter(4,[40.0/(fsamp/2),250.0/(fsamp/2)],'bandpass')


	b1 = scisig.lfilter(B,A,sig1[:500:-1])
	b2 = scisig.lfilter(B,A,sig2[:500:-1])
	b3 = scisig.lfilter(B,A,sig3[:500:-1])

	b1 = b1[::-1]
	b2 = b2[::-1]
	b3 = b3[::-1]

	b11 =  scisig.lfilter(B,A,sig1[0:500])
	b22 =  scisig.lfilter(B,A,sig2[0:500])
	b33 =  scisig.lfilter(B,A,sig3[0:500])

	c1 = np.hstack((b11,b1))
	c2 = np.hstack((b22,b2))
	c3 = np.hstack((b33,b3))


	VM = np.sqrt(np.square(c1)+np.square(c2)+np.square(c3))
	print 'late potentials'
	
	vc = VM.tolist()
	
	return vc

