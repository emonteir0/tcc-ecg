from peewee import *
import json
import datetime
import commands

db = SqliteDatabase('ecg.sqlite')


class users(Model):
	username = CharField()
	password = CharField()
    
	class Meta:
		database = db

class pacients(Model):
	name = CharField()
	gender = CharField()
	birthday = CharField()
	rg = CharField()
	cpf = CharField()
	device = CharField()
	exam = CharField()
	comment = CharField()
	date = DateField()
	
	class Meta:
		database = db


def userexists(username):
	return len(users.select().where(users.username == username))
	
def requestlogin(username, password):
	return len(users.select().where(users.username == username and users.password == password))

def getpacients(prefix):
	return pacients.select().where(pacients.name.startswith(prefix)).order_by(pacients.name, pacients.date.desc())
	
def savepacient():
	f = open('temp/user.txt')
	userinformation = json.load(f)
	f.close()
	pacient = pacients()
	pacient.name = userinformation['Name']
	pacient.gender = userinformation['Gender']
	pacient.birthday = userinformation['Birthday']
	pacient.rg = userinformation['RG']
	pacient.cpf = userinformation['CPF']
	pacient.device = userinformation['Device']['Name']	
	pacient.exam = userinformation['Exam']['Name']
	pacient.comment = userinformation['Comment']
	pacient.date = datetime.datetime.now().isoformat()
	pacient.save()
	commands.getoutput('mkdir pacients/{0}'.format(pacient.id))
	commands.getoutput('cp temp/* pacients/{0}'.format(pacient.id))
	
