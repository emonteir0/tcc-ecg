import sys
import json
import struct
import datetime
import time
import subprocess
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from ads1298acq import ADS1298Acquisition

import numpy as npy
import matplotlib
matplotlib.use('Qt4Agg')
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt

class Window():
	def __init__(self):
		#Janela
		self.xstart = 300
		self.window = QDialog()
		self.window.resize(1360, 768)
		self.window.setWindowTitle('ADS1298 - Aquisicao')
		self.window.finished.connect(self.endapp)
		
		#Titulo
		self.title = QLabel('ADS1298 - Aquisicao', self.window)
		font = QFont()
		font.setPointSize(40)
		font.setBold(True)
		self.title.setFont(font)
		self.title.move(400, 30)
		
		#Segundos
		self.spinbox = QSpinBox(self.window)
		self.spinbox.setMaximum(3600)
		self.spinbox.setMinimum(1)
		self.spinbox.move(500, 160)
		self.spinboxtitle = QLabel('Segundos', self.window)
		self.spinboxtitle.move(500, 140)
		#Record Button
		self.recordbutton = QPushButton('Gravar', self.window)
		self.recordbutton.move(600, 160)
		self.recordbutton.clicked.connect(self.record)
		#Progress Bar
		self.progress = QProgressBar(self.window)
		self.progress.move(800, 160)
		self.progress.setMinimum(0)
		self.progress.setMaximum(10)
		self.progress.setValue(0)
		self.progress.resize(540, 30)
		#Progress Text
		self.progresstext = QLabel('Aguardando...', self.window)
		self.progresstext.move(800, 140)
		#Botao voltar
		self.backbutton = QPushButton('Voltar', self.window)
		self.backbutton.move(30, 30)
		self.backbutton.clicked.connect(self.back)
		#Botao avancar
		self.forwardbutton = QPushButton('Avancar', self.window)
		self.forwardbutton.move(1250, 30)
		self.forwardbutton.clicked.connect(self.forward)
		
		
		#Variaveis
		self.graphs = [[], [], [], [], [], [], [], []]
		self.rawdata = []
		self.data = None
		plt.style.use('ggplot')
		self.figure = plt.figure()
		for i in range(0, 8):
			self.graphs[i] = self.figure.add_subplot(811+i)
			self.graphs[i].grid(color='orange', linestyle = '-', linewidth = 1)
		self.ADS1298 = ADS1298Acquisition()
		self.action = 0
		#Canvas
		self.canvas = FigureCanvas(self.figure)
		self.canvas.resize(1360, 1200)
		#self.canvas.setParent(self.window)
		self.scroll = QScrollArea(self.window)
		self.scroll.setWidget(self.canvas)
		self.scroll.resize(1360, 560)
		self.scroll.move(0, 200)
		
		self.window.show()
		self.plot(self.ADS1298.convert(self.ADS1298.getSample(4000)))
	
	def endapp(self, event):
		app.exit()
	
	def back(self, event):
		self.action = 1
		app.exit()
		
	def forward(self, event):
		self.action = 2
		out_file = open('temp/data.dat',"wb")
		for i in range(0, len(self.data[0])):
			s = struct.pack('dddddddd', self.data[0][i], self.data[1][i], self.data[2][i], self.data[3][i], self.data[4][i], self.data[5][i], self.data[6][i], self.data[7][i])
			out_file.write(s)
		out_file.close()
		app.exit()
	
	def record(self, event):
		limit = self.spinbox.value()
		self.progresstext.setText('Adquirindo...')
		self.progress.setMinimum(0)
		self.progress.setMaximum(limit)
		self.progress.setValue(0)
		self.rawdata = []
		self.data = [[], [], [], [], [], [], [], []]
		N = 0
		while N < limit:
			self.acquire(1000)
			#x = datetime.datetime.now()
			N += 1
			#self.progress.setValue(N)
			#print datetime.datetime.now() - x
		self.progresstext.setText('Convertendo...')
		self.progress.setValue(0.7*limit)
		time.sleep(2)
		self.data = self.ADS1298.convert(self.rawdata)
		self.progress.setValue(limit)
		self.progresstext.setText('Finalizado!')
		self.plot(self.data)
			
	def acquire(self, sampletime):
		sample = self.ADS1298.getSample(sampletime)
		self.rawdata += sample
		return sample
		
	def plot(self, sample):
		for i in range(0, 8):
			self.graphs[i].remove()
			self.graphs[i] = self.figure.add_subplot(811+i)
			self.graphs[i].grid(color='orange', linestyle = '-', linewidth = 1)
			self.graphs[i].plot(sample[i][0:10000], color='red')
		self.canvas.draw()
	
	

		
if __name__ == '__main__':
	app = QApplication(sys.argv)
	main = Window()
	app.exec_()
	main.window.close()
	main.ADS1298.stop()
	f = open('temp/user.txt')
	userinformation = json.load(f)
	f.close()
	if main.action == 1:
		subprocess.Popen('python main.py', shell = True)
	elif main.action == 2:
		subprocess.Popen('python ' + userinformation['Exam']['File'], shell = True)
	sys.exit()
