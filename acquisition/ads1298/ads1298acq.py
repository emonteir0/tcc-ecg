import RPi.GPIO as GPIO
from periphery import SPI
import time
import datetime

PIN_RESET = 40
PIN_START = 37
PIN_DRDY = 38
PIN_CS = 36


class ADS1298Acquisition():
	
	def __init__(self):
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(PIN_START, GPIO.OUT)
		GPIO.setup(PIN_RESET, GPIO.OUT)
		GPIO.setup(PIN_CS, GPIO.OUT)
		GPIO.setup(PIN_DRDY, GPIO.IN, pull_up_down = GPIO.PUD_UP)
		GPIO.output(PIN_START, 0)
		GPIO.output(PIN_CS, 1)
		GPIO.output(PIN_RESET, 0)
		time.sleep(1)
		GPIO.output(PIN_RESET, 1)
		self.spi = SPI("/dev/spidev0.0", 0, 8000000)
		self.spi.mode = 1
		time.sleep(1)
		self.adc_send_command(0x11)
		time.sleep(0.1)
		self.adc_wreg(0x01, 0x85)
		self.adc_wreg(0x02, 0x00)
		self.adc_wreg(0x03, 0xDC)
		self.adc_wreg(0x04, 0x13)
		self.adc_wreg(0x05, 0x00)
		self.adc_wreg(0x06, 0x00)
		self.adc_wreg(0x07, 0x00)
		self.adc_wreg(0x08, 0x00)
		self.adc_wreg(0x09, 0x00)
		self.adc_wreg(0x0A, 0x00)
		self.adc_wreg(0x0B, 0x00)
		self.adc_wreg(0x0C, 0x00)
		self.adc_wreg(0x0D, 0x06)
		self.adc_wreg(0x0E, 0x02)
		self.adc_wreg(0x0F, 0xFF)
		self.adc_wreg(0x10, 0x02)
		self.adc_wreg(0x11, 0x00)
		self.adc_wreg(0x14, 0x0F)
		self.adc_wreg(0x15, 0x00)
		self.adc_wreg(0x16, 0x00)
		self.adc_wreg(0x17, 0x02)
		self.adc_wreg(0x18, 0x0A)
		self.adc_wreg(0x19, 0xE3)
		
		
		self.pacote = 27 * [0]
		
	def adc_send_command(self, cmd):
		GPIO.output(PIN_CS, 0)
		self.spi.transfer([cmd])
		time.sleep(2e-6)
		GPIO.output(PIN_CS, 1)
		time.sleep(10e-6)

	def adc_wreg(self, reg, val):
		GPIO.output(PIN_CS, 0)
		self.spi.transfer([0x40+reg])
		self.spi.transfer([0])
		out = self.spi.transfer([val])
		time.sleep(10e-6)
		GPIO.output(PIN_CS, 1)
		

	def adc_rreg(self, reg):
		out = 0
		GPIO.output(PIN_CS, 0)
		self.spi.transfer([0x20+reg])
		self.spi.transfer([0])
		time.sleep(1e-6)
		out = self.spi.transfer([0])
		time.sleep(10e-6)
		GPIO.output(PIN_CS, 1)
		return out
		
		
	def getSample(self, sampletime):
		R = []
		self.adc_send_command(0x08)
		self.adc_send_command(0x10)
		N = 0
		t0 = datetime.datetime.now()
		while N < sampletime:
			if GPIO.input(PIN_DRDY) == 0:
				GPIO.output(PIN_CS, 0)
				R += self.spi.transfer(self.pacote)
				GPIO.output(PIN_CS, 1)
				N += 1
		print len(R)
		self.adc_send_command(0x0A)
		return R
		
	def convert(self, R):
		m = [[], [], [], [], [], [], [], []]
		tam = len(R)/27
		print tam
		t0 = datetime.datetime.now()
		for i in range(0, tam):
			for j in range(1, 9):
				x = i*27
				m[j-1].append((R[x+3*j] << 16) + (R[x+3*j+1] << 8) + R[x+3*j+2])
		print datetime.datetime.now() - t0
		return m

	def stop(self):
		self.spi.close()
		GPIO.cleanup()
		
		
		
if __name__ == '__main__':	
	ADS1298 = ADS1298Acquisition()
	ADS1298.getSample(1000)
	ADS1298.stop()
