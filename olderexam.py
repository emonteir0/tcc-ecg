import sys
import json
import struct
import time
import commands
import datetime
import dateutil
from database import getpacients
import subprocess
from PyQt4.QtGui import *
from PyQt4.QtCore import *

def birthdayconvert(string):
	v = string.split(' ')
	return '{0}/{1}/{2}'.format(v[2], v[1], v[0])
	
def dateconvert(timestamp):
	dt, _, us= timestamp.partition(".")
	dt= datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S")
	us= int(us.rstrip("Z"), 10)
	date = dt + datetime.timedelta(microseconds=us)
	return date.strftime('%d/%m/%Y %H:%M:%S')
	

class Window():
	def __init__(self):
		#Janela
		self.xstart = 300
		self.window = QDialog()
		self.window.resize(1360, 768)
		self.window.setWindowTitle('Exames Antigos')
		self.window.finished.connect(self.endapp)
		self.action = 0
		#Titulo
		self.title = QLabel('Exames Antigos', self.window)
		font = QFont()
		font.setPointSize(40)
		font.setBold(True)
		self.title.setFont(font)
		self.title.move(500, 40)
		#Botao voltar
		self.backbutton = QPushButton('Voltar', self.window)
		self.backbutton.move(60, 60)
		self.backbutton.clicked.connect(self.backbuttonclick)
		#Botao avancar
		self.backbutton = QPushButton('Avancar', self.window)
		self.backbutton.move(1200, 60)
		#Tabela
		self.table = QTableWidget(self.window)
		self.table.setColumnCount(8)
		self.table.setRowCount(30)
		self.table.setColumnWidth(0, 250)
		self.table.setColumnWidth(1, 150)
		self.table.setColumnWidth(2, 100)
		self.table.setColumnWidth(3, 100)
		self.table.setColumnWidth(4, 100)
		self.table.setColumnWidth(5, 150)
		self.table.setColumnWidth(6, 200)
		self.table.setColumnWidth(7, 200)
		self.table.move(50, 200)
		self.table.resize(1250, 500)
		self.table.verticalHeader().hide()
		self.table.setHorizontalHeaderLabels(['Nome', 'Data de Nascimento', 'RG', 'CPF', 'Genero', 'Data do Exame', 'Equipamento Utilizado', 'Exame Realizado'])
		self.fill('')
		
		#Pesquisa
		self.pesquisa = QLineEdit(self.window)
		self.pesquisa.resize(250, 30)
		self.pesquisa.move(50, 160)
		self.pesquisatitle = QLabel('Pesquisar Nome', self.window)
		self.pesquisatitle.move(50, 140)
		self.pesquisa.textChanged.connect(self.search)
	
	def backbuttonclick(self, event):
		self.action = 1
		app.exit()
	
	def search(self, event):
		self.fill(self.pesquisa.text())

	def fill(self, prefix):
		X = getpacients(prefix)
		self.table.setRowCount(len(X))
		i = 0
		for x in X:
			self.table.setItem(i, 0, QTableWidgetItem(x.name))
			self.table.setItem(i, 1, QTableWidgetItem(birthdayconvert(x.birthday)))
			self.table.setItem(i, 2, QTableWidgetItem(x.rg))
			self.table.setItem(i, 3, QTableWidgetItem(x.cpf))
			self.table.setItem(i, 4, QTableWidgetItem(x.gender))
			self.table.setItem(i, 5, QTableWidgetItem(dateconvert(x.date)))
			self.table.setItem(i, 6, QTableWidgetItem(x.device))
			self.table.setItem(i, 7, QTableWidgetItem(x.exam))
			i += 1
	
	def endapp(self, event):
		app.exit()
				
	
	
		
if __name__ == '__main__':
	app = QApplication(sys.argv)
	main = Window()
	main.window.show()
	app.exec_()
	main.window.close()
	if main.action == 1:
		subprocess.Popen('python selection.py', shell = True)
	sys.exit()
